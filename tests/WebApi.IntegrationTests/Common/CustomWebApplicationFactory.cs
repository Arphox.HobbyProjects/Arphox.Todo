﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Persistence;

namespace WebApi.IntegrationTests.Common;

public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<TStartup>
    where TStartup : class
{
    public BurntInObjects BurntInObjects { get; private set; } = default!;

    protected override void ConfigureWebHost(IWebHostBuilder builder)
    {
        builder.ConfigureServices(services =>
        {
            RemoveRealDbContextRegistration(services);
            RegisterInMemoryDb(services);
            InitializeDb(services);
        })
            .UseEnvironment("IntegrationTest");
    }

    private static void RemoveRealDbContextRegistration(IServiceCollection services)
    {
        var descriptor = services.Single(
            d => d.ServiceType == typeof(DbContextOptions<TodoDbContext>));

        services.Remove(descriptor);
    }

    private static void RegisterInMemoryDb(IServiceCollection services)
    {
        var guid = Guid.NewGuid();
        services.AddDbContext<TodoDbContext>(options =>
        {
            options.UseInMemoryDatabase($"InMemoryDb-{guid}");
        });
    }

    private void InitializeDb(IServiceCollection services)
    {
        var sp = services.BuildServiceProvider();
        using var scope = sp.CreateScope();
        var scopedServices = scope.ServiceProvider;
        var context = scopedServices.GetRequiredService<TodoDbContext>();
        context.Database.EnsureCreated();

        BurntInObjects = DbSeeder.Seed(context);
    }
}
