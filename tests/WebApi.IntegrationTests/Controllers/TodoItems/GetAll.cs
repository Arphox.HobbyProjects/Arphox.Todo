﻿using Domain.Entities;
using FluentAssertions;
using System.Net;
using WebApi.IntegrationTests.Common;
using Xunit;
using static WebApi.IntegrationTests.Common.Utilities;

namespace WebApi.IntegrationTests.Controllers.TodoItems;

public sealed class GetAll : IClassFixture<CustomWebApplicationFactory<TestPlaceholderType>>
{
    private readonly CustomWebApplicationFactory<TestPlaceholderType> _factory;

    public GetAll(CustomWebApplicationFactory<TestPlaceholderType> factory)
        => _factory = factory ?? throw new ArgumentNullException(nameof(factory));

    [Fact]
    public async Task GetAll_ReturnsAllItems()
    {
        // Arrange
        var client = _factory.CreateClient();

        // Act
        var response = await client.GetAsync($"/api/todoitems/getall");

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
        var items = await GetResponseContent<TodoItem[]>(response);
        items.Should().BeEquivalentTo(new[]
        {
            _factory.BurntInObjects.UncompletedItem,
            _factory.BurntInObjects.CompletedItem
        }, cfg => cfg.WithoutStrictOrdering());
    }
}
