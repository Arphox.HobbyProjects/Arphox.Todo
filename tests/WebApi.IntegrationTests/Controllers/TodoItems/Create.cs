﻿using Application.TodoItems.Commands.CreateTodoItem;
using FluentAssertions;
using System.Net;
using WebApi.IntegrationTests.Common;
using Xunit;
using static WebApi.IntegrationTests.Common.Utilities;

namespace WebApi.IntegrationTests.Controllers.TodoItems;

public sealed class Create : IClassFixture<CustomWebApplicationFactory<TestPlaceholderType>>
{
    private const string RequestUri = "/api/todoitems/create";
    private readonly CustomWebApplicationFactory<TestPlaceholderType> _factory;

    public Create(CustomWebApplicationFactory<TestPlaceholderType> factory)
        => _factory = factory ?? throw new ArgumentNullException(nameof(factory));

    [Fact]
    public async Task CreatingValidItem_Succeeds()
    {
        // Arrange
        var client = _factory.CreateClient();

        var command = new CreateTodoItemCommand
        {
            Title = "Hey Jude, don't let me down!"
        };

        var content = GetRequestContent(command);

        // Act
        var response = await client.PostAsync(RequestUri, content);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.Created);
        var itemId = await GetResponseContent<int>(response);
        itemId.Should().NotBe(0);

        response.Headers.Location.Should().NotBeNull();
        var location = response.Headers.Location!.AbsoluteUri;
        location.Should().Be($"http://localhost/api/TodoItems/Get/{itemId}");

        var getResponse = await client.GetAsync(location);
        getResponse.StatusCode.Should().Be(HttpStatusCode.OK);
    }

    [Fact]
    public async Task CreatingInvalidItem_Fails()
    {
        // Arrange
        var client = _factory.CreateClient();

        var command = new CreateTodoItemCommand
        {
            Title = "" // <- invalid
        };

        var content = GetRequestContent(command);

        // Act
        var response = await client.PostAsync(RequestUri, content);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        var failures = await GetResponseContent<Dictionary<string, string[]>>(response);
        failures.Should().HaveCount(1);
        failures.Should().ContainKey("Title").WhoseValue.Should().ContainSingle();
        failures["Title"].Single().Should().Be("'Title' must not be empty.");
    }
}
