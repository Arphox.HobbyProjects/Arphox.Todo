﻿using FluentAssertions;
using System.Net;
using WebApi.IntegrationTests.Common;
using Xunit;

namespace WebApi.IntegrationTests.Controllers.TodoItems;

public sealed class Delete : IClassFixture<CustomWebApplicationFactory<TestPlaceholderType>>
{
    private readonly CustomWebApplicationFactory<TestPlaceholderType> _factory;

    public Delete(CustomWebApplicationFactory<TestPlaceholderType> factory)
        => _factory = factory ?? throw new ArgumentNullException(nameof(factory));

    [Fact]
    public async Task DeletingExistingItem_Succeeds()
    {
        // Arrange
        var client = _factory.CreateClient();
        var id = _factory.BurntInObjects.CompletedItem.Id;

        var getResponse = await client.GetAsync($"/api/todoitems/get/{id}");
        getResponse.StatusCode.Should().Be(HttpStatusCode.OK);

        // Act
        var deleteResponse = await client.DeleteAsync($"/api/todoitems/delete/{id}");

        // Assert
        deleteResponse.StatusCode.Should().Be(HttpStatusCode.NoContent);

        getResponse = await client.GetAsync($"/api/todoitems/get/{id}");
        getResponse.StatusCode.Should().Be(HttpStatusCode.NotFound);
    }

    [Fact]
    public async Task DeletingNotexistingItem_Fails()
    {
        // Arrange
        var client = _factory.CreateClient();
        var id = 4_663_743_464L; // random, not exists

        // Act
        var response = await client.DeleteAsync($"/api/todoitems/delete/{id}");

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.NotFound);
    }
}
