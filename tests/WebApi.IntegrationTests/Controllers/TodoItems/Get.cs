﻿using Domain.Entities;
using FluentAssertions;
using System.Net;
using WebApi.IntegrationTests.Common;
using Xunit;
using static WebApi.IntegrationTests.Common.Utilities;

namespace WebApi.IntegrationTests.Controllers.TodoItems;

public sealed class Get : IClassFixture<CustomWebApplicationFactory<TestPlaceholderType>>
{
    private readonly CustomWebApplicationFactory<TestPlaceholderType> _factory;

    public Get(CustomWebApplicationFactory<TestPlaceholderType> factory)
        => _factory = factory ?? throw new ArgumentNullException(nameof(factory));

    [Fact]
    public async Task GettingExistingItem_ReturnsItem()
    {
        // Arrange
        var client = _factory.CreateClient();
        var id = _factory.BurntInObjects.UncompletedItem.Id;

        // Act
        var response = await client.GetAsync($"/api/todoitems/get/{id}");

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
        var item = await GetResponseContent<TodoItem>(response);
        item.Should().BeEquivalentTo(_factory.BurntInObjects.UncompletedItem);
    }

    [Fact]
    public async Task GettingNotexistingItem_Fails()
    {
        // Arrange
        var client = _factory.CreateClient();
        var id = 4_663_743_464L; // random, not exists

        // Act
        var response = await client.GetAsync($"/api/todoitems/get/{id}");

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.NotFound);
    }
}
