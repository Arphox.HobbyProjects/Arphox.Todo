﻿using Application.TodoItems.Commands.UpdateTodoItem;
using Domain.Entities;
using FluentAssertions;
using System.Net;
using WebApi.IntegrationTests.Common;
using Xunit;
using static WebApi.IntegrationTests.Common.Utilities;

namespace WebApi.IntegrationTests.Controllers.TodoItems;

public sealed class Update : IClassFixture<CustomWebApplicationFactory<TestPlaceholderType>>
{
    private const string RequestUri = "/api/todoitems/update";
    private readonly CustomWebApplicationFactory<TestPlaceholderType> _factory;

    public Update(CustomWebApplicationFactory<TestPlaceholderType> factory)
        => _factory = factory ?? throw new ArgumentNullException(nameof(factory));

    [Fact]
    public async Task UpdatingExistingItem_Succeeds()
    {
        // Arrange
        var client = _factory.CreateClient();
        var newTitle = _factory.BurntInObjects.UncompletedItem.Title;
        var newIsCompleted = !_factory.BurntInObjects.UncompletedItem.IsComplete;

        var command = new UpdateTodoItemCommand
        {
            Id = 1,
            Title = newTitle,
            IsComplete = newIsCompleted,
        };

        var content = GetRequestContent(command);

        // Act
        var response = await client.PutAsync(RequestUri, content);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
        var gotItem = await GetResponseContent<TodoItem>(response);
        gotItem.Should().BeEquivalentTo(command);
    }

    [Fact]
    public async Task UpdatingExistingItemWithInvalidData_Fails()
    {
        // Arrange
        var client = _factory.CreateClient();

        var command = new UpdateTodoItemCommand
        {
            Id = 1,
            Title = "", // <- invalid
            IsComplete = true,
        };

        var content = GetRequestContent(command);

        // Act
        var response = await client.PutAsync(RequestUri, content);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
    }

    [Fact]
    public async Task UpdatingNotExistingItem_Fails()
    {
        // Arrange
        var client = _factory.CreateClient();

        var command = new UpdateTodoItemCommand
        {
            Id = 1234, // <- does not exist
            Title = "Do something",
            IsComplete = false,
        };

        var content = GetRequestContent(command);

        // Act
        var response = await client.PutAsync(RequestUri, content);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.NotFound);
    }
}
