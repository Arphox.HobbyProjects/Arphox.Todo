﻿using WebApi.IntegrationTests.Common;
using Xunit;

namespace WebApi.IntegrationTest;

public sealed class BasicTests : IClassFixture<CustomWebApplicationFactory<TestPlaceholderType>>
{
    private readonly CustomWebApplicationFactory<TestPlaceholderType> _factory;

    public BasicTests(CustomWebApplicationFactory<TestPlaceholderType> factory)
        => _factory = factory ?? throw new ArgumentNullException(nameof(factory));

    [Fact]
    public async Task GetRoot_Success()
    {
        // Arrange
        var client = _factory.CreateClient();

        // Act
        var response = await client.GetAsync("/");

        // Assert
        response.EnsureSuccessStatusCode();
    }
}
