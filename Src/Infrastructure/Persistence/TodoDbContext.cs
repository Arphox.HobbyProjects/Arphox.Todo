﻿using Application.Common.Interfaces;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Persistence;

public sealed class TodoDbContext : DbContext, ITodoDbContext
{
    public TodoDbContext(DbContextOptions<TodoDbContext> options)
        : base(options)
    { }

    public DbSet<TodoItem> TodoItems { get; set; }
}