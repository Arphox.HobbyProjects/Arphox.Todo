﻿using Microsoft.EntityFrameworkCore;

namespace Persistence;

public sealed class TodoDbContextFactory : DesignTimeDbContextFactoryBase<TodoDbContext>
{
    protected override TodoDbContext CreateNewInstance(DbContextOptions<TodoDbContext> options)
    {
        return new TodoDbContext(options);
    }
}