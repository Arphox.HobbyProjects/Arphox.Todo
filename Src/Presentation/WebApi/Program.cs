﻿using Application;
using Microsoft.EntityFrameworkCore;
using Persistence;
using System.Diagnostics;
using WebApi.Common;
using WebApi.GraphQL;
using WebApi.Setup;

WebApplication app = CreateApp(args);
ConfigureApp(app);
SetupDb(app);
await app.RunAsync();



static WebApplication CreateApp(string[] args)
{
    var builder = WebApplication.CreateBuilder(args);
    var services = builder.Services;
    services
        .AddPersistence(builder.Configuration)
        .AddApplication()
        .AddEndpointsApiExplorer()
        .AddSwagger();

    services.SetupGraphQL();

    services.AddControllers();
    return builder.Build();
}

static void ConfigureApp(WebApplication app)
{
    if (app.Environment.IsDevelopment())
    {
        app.UseSwagger();
        app.UseSwaggerUI();
    }

    app.UseCustomExceptionHandler();
    app.Map("/", () => "");
    app.MapControllers();

    app.UseRouting().UseEndpoints(endpoints => endpoints.MapGraphQL());
}

static void SetupDb(WebApplication app)
{
    if (app.Environment.EnvironmentName == "IntegrationTest")
        return;

    using var scope = app.Services.CreateScope();
    var services = scope.ServiceProvider;
    try
    {
        var context = services.GetRequiredService<TodoDbContext>();
        context.Database.Migrate();
    }
    catch (Exception)
    {
        Debugger.Break();
        throw;
        // TODO log
    }
}

public sealed class TestPlaceholderType { } // Integration tests need a type in this assembly
