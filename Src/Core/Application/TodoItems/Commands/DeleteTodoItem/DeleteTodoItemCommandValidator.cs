﻿using FluentValidation;

namespace Application.TodoItems.Commands.DeleteTodoItem;

public sealed class DeleteTodoItemCommandValidator : AbstractValidator<DeleteTodoItemCommand>
{
}
