﻿using FluentValidation;
using static Application.TodoItems.Commands.Common.CommonTodoItemValidators;

namespace Application.TodoItems.Commands.CreateTodoItem;

internal sealed class CreateTodoItemCommandValidator : AbstractValidator<CreateTodoItemCommand>
{
    public CreateTodoItemCommandValidator()
    {
        ValidateTodoItemTitle(RuleFor(x => x.Title));
    }
}
