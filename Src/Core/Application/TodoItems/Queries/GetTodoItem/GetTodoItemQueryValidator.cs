﻿using Application.TodoItems.Queries.GetTodoItem;
using FluentValidation;

namespace Application.TodoItems.Commands.CreateTodoItem;

internal sealed class GetTodoItemQueryValidator : AbstractValidator<GetTodoItemQuery>
{
    public GetTodoItemQueryValidator()
    {
    }
}
