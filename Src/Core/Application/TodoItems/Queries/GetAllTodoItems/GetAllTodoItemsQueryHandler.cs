﻿using Application.Common.Interfaces;
using Application.TodoItems.Queries.GetTodoItem;
using Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.TodoItems.Queries.GetAllTodoItems;

public sealed class GetAllTodoItemsQueryHandler : IRequestHandler<GetAllTodoItemsQuery, IList<TodoItem>>
{
    private readonly ITodoDbContext _context;

    public GetAllTodoItemsQueryHandler(ITodoDbContext context)
    {
        _context = context ?? throw new ArgumentNullException(nameof(context));
    }

    public async Task<IList<TodoItem>> Handle(GetAllTodoItemsQuery request, CancellationToken cancellationToken)
    {
        return await _context.TodoItems
            .AsNoTracking()
            .ToListAsync(cancellationToken);
    }
}
