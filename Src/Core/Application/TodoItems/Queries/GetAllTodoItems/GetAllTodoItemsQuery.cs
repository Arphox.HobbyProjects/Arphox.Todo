﻿using Domain.Entities;
using MediatR;

namespace Application.TodoItems.Queries.GetTodoItem;

/// <remarks>Handler: <see cref="GetAllTodoItemsQueryHandler"/></remarks>
public sealed class GetAllTodoItemsQuery : IRequest<IList<TodoItem>>
{
}
