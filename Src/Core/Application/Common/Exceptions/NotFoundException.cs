﻿
namespace Application.Common.Exceptions;

public sealed class NotFoundException<TEntity> : NotFoundException
{
    public NotFoundException(object key)
#pragma warning disable CS0618 // Type or member is obsolete
        : base($"Entity '{typeof(TEntity).Name}' (key='{key}') was not found.")
#pragma warning restore CS0618 // Type or member is obsolete
    { }
}

public abstract class NotFoundException : Exception
{
    [Obsolete("Use the generic type, please")]
    public NotFoundException(string message)
        : base(message)
    { }
}
